package com.siriphonpha.application2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.siriphonpha.application2.databinding.FragmentPlusBinding

class PlusFragment : Fragment() {
    private var _binding: FragmentPlusBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentPlusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textView4.setOnClickListener {
            val numberone = binding.textView.text.toString().toInt()
            val numbertwo = binding.textView2.text.toString().toInt()
            val answer = numberone + numbertwo
            val action =
                PlusFragmentDirections.actionPlusFragmentToAnswerPlusFragment(answer = answer.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}