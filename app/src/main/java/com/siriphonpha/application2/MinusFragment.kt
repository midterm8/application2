package com.siriphonpha.application2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.siriphonpha.application2.databinding.FragmentMinusBinding

class MinusFragment : Fragment() {
    private var _binding: FragmentMinusBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMinusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.EqualMinus.setOnClickListener {
            val number1 = binding.textView1.text.toString().toInt()
            val number2 = binding.textView2.text.toString().toInt()
            val answerminus = number1 - number2
            val action =
                MinusFragmentDirections.actionMinusFragmentToAnswerFragment(answerminus = answerminus.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}